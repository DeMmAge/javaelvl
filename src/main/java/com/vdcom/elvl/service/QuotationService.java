package com.vdcom.elvl.service;

import com.vdcom.elvl.domain.Elvl;
import com.vdcom.elvl.domain.Quotation;
import com.vdcom.elvl.repo.ElvlRepo;
import com.vdcom.elvl.repo.QuotationRepo;
import com.vdcom.elvl.service.exception.BadQuotationException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Vadim Guseynov
 */

@Service
@RequiredArgsConstructor
public class QuotationService {

    private final QuotationRepo quotationRepo;
    private final ElvlRepo elvlRepo;

    public void acceptQuotation(Quotation quotation) {
        validateQuotation(quotation);

        Elvl elvlFromDb = elvlRepo.findByIsin(quotation.getIsin());
        if (elvlFromDb == null) {
            float value;
            if (quotation.getBid() != 0F) {
                value = quotation.getBid();
            } else {
                value = quotation.getAsk();
            }
            elvlRepo.save(new Elvl(quotation.getIsin(), value));
        } else {
            if (quotation.getBid() > elvlFromDb.getValue()) {
                elvlFromDb.setValue(quotation.getBid());
            } else if (quotation.getAsk() < elvlFromDb.getValue()) {
                elvlFromDb.setValue(quotation.getAsk());
            }
            elvlRepo.save(elvlFromDb);
        }
        quotationRepo.save(quotation);
    }

    private void validateQuotation(Quotation quotation) {
        if (quotation.getBid() >= quotation.getAsk()) {
            throw new BadQuotationException("Bid must be less than ask");
        } else if (quotation.getIsin().length() != 12) {
            throw new BadQuotationException("Isin length must be equal to 12 characters");
        } else if (quotation.getAsk() == 0F && quotation.getBid() == 0F) {
            throw new BadQuotationException("Bid and ask can't be empty");
        }
    }

    public Elvl getElvlByIsin(String isin) {
        return elvlRepo.findByIsin(isin);
    }
}
