package com.vdcom.elvl.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Vadim Guseynov
 */

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadQuotationException extends RuntimeException {
    public BadQuotationException(String message) {
        super(message);
    }
}
