package com.vdcom.elvl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElvlJavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElvlJavaApplication.class, args);
    }

}
