package com.vdcom.elvl.repo;

import com.vdcom.elvl.domain.Elvl;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Vadim Guseynov
 */

public interface ElvlRepo extends JpaRepository<Elvl, String> {

    Elvl findByIsin(String isin);

}
