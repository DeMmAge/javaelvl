package com.vdcom.elvl.repo;

import com.vdcom.elvl.domain.Quotation;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Vadim Guseynov
 */

public interface QuotationRepo extends JpaRepository<Quotation, Long> {
}
