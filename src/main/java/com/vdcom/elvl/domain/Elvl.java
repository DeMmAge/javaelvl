package com.vdcom.elvl.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author Vadim Guseynov
 */

@Data
@Entity
@Table(name = "elvls")
@AllArgsConstructor
@NoArgsConstructor
public class Elvl {

    @Id
    @Column(unique = true)
    private String isin;

    private float value;
}
