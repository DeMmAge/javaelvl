package com.vdcom.elvl.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

/**
 * @author Vadim Guseynov
 */

@Data
@Entity
@Table(name = "quotations")
@AllArgsConstructor
@NoArgsConstructor
public class Quotation {

    @Id
    @GeneratedValue
    private Long id;

    @JsonProperty("isin")
    @Column(length = 12)
    private String isin;

    @JsonProperty("bid")
    private float bid;

    @JsonProperty("ask")
    private float ask;
}
