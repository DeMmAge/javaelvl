package com.vdcom.elvl.controller;

import com.vdcom.elvl.service.exception.BadQuotationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author Vadim Guseynov
 */

@RestControllerAdvice
public class ControllerAdviser {

    @ExceptionHandler(value = BadQuotationException.class)
    public ResponseEntity<String> handleBadQuotation(BadQuotationException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
