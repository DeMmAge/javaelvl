package com.vdcom.elvl.controller;

import com.vdcom.elvl.domain.Elvl;
import com.vdcom.elvl.domain.Quotation;
import com.vdcom.elvl.service.QuotationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Vadim Guseynov
 */

@RestController
@RequiredArgsConstructor
public class QuotationController {

    private final QuotationService service;

    @PostMapping("/postAll")
    public void acceptQuotations(@RequestBody List<Quotation> quotations) {
        for (Quotation quotation : quotations) {
            service.acceptQuotation(quotation);
        }
    }

    @PostMapping
    public void acceptQuotations(@RequestBody Quotation quotation) {
        service.acceptQuotation(quotation);
    }

    @GetMapping("{isin}")
    public ResponseEntity<Elvl> getElvlByIsin(@PathVariable String isin) {
        Elvl elvl = service.getElvlByIsin(isin);

        if (elvl != null) {
            return new ResponseEntity<>(elvl, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
