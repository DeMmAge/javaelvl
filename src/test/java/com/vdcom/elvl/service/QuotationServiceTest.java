package com.vdcom.elvl.service;

import com.vdcom.elvl.domain.Quotation;
import com.vdcom.elvl.service.exception.BadQuotationException;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
class QuotationServiceTest {

    @Autowired
    private QuotationService quotationService;

    @Test
    void shouldThrowBadQuotationExceptionThenQuotationWithSimilarAskAndBidGiven() {
        Assertions.assertThrows(BadQuotationException.class, () -> {
            quotationService.acceptQuotation(new Quotation(999L, "RU000N23GXA9", 100.2F, 100.2F));
        });
    }

    @Test
    void shouldThrowBadQuotationExceptionThenQuotationMissingAskAndBid() {
        Assertions.assertThrows(BadQuotationException.class, () -> {
            quotationService.acceptQuotation(new Quotation(999L, "RU000N23GXA9", 0F, 0F));
        });
    }

    @Order(1)
    @Test
    void shouldAcceptQuotationAndCreateElvl() {
        quotationService.acceptQuotation(new Quotation(999L, "RU000N23GXA9", 100F, 120F));
    }

    @Order(2)
    @Test
    void shouldReturnSavedElvl() {
        assertNotNull(quotationService.getElvlByIsin("RU000N23GXA9"));
    }
}