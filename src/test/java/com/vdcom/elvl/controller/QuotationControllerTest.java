package com.vdcom.elvl.controller;

import lombok.SneakyThrows;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
class QuotationControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    @Order(1)
    @SneakyThrows
    void shouldReturnIsinWithBidValue() {
        mvc.perform(post("/")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"isin\": \"RU000A102XF1\",\n" +
                        "    \"bid\": 100.2,\n" +
                        "    \"ask\": 101.9 \n" +
                        "}")
                .characterEncoding("UTF-8"))
                .andExpect(status().isOk());

        mvc.perform(get("/RU000A102XF1"))
                .andExpect(status().isOk())
                .andExpect(
                        content().json(
                                "{\n" +
                                        "    \"isin\": \"RU000A102XF1\",\n" +
                                        "    \"value\": 100.2\n" +
                                        "}"
                        )
                );
    }

    @Test
    @Order(2)
    @SneakyThrows
    void shouldUpdateElvlWithNewBidValue() {
        mvc.perform(post("/")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"isin\": \"RU000A102XF1\",\n" +
                        "    \"bid\": 100.5,\n" +
                        "    \"ask\": 101.9 \n" +
                        "}")
                .characterEncoding("UTF-8"))
                .andExpect(status().isOk());

        mvc.perform(get("/RU000A102XF1"))
                .andExpect(status().isOk())
                .andExpect(
                        content().json(
                                "{\n" +
                                        "    \"isin\": \"RU000A102XF1\",\n" +
                                        "    \"value\": 100.5\n" +
                                        "}"
                        )
                );
    }

    @Test
    @Order(3)
    @SneakyThrows
    void shouldAcceptMultipleQuotations() {
        mvc.perform(post("/postAll")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{\n" +
                        "    \"isin\": \"RU000A102XF2\",\n" +
                        "    \"bid\": 130,\n" +
                        "    \"ask\": 140 \n" +
                        "},\n" +
                        "\n" +
                        "{\n" +
                        "    \"isin\": \"RU000A102XF3\",\n" +
                        "    \"bid\": 140,\n" +
                        "    \"ask\": 150 \n" +
                        "},\n" +
                        "\n" +
                        "{\n" +
                        "    \"isin\": \"RU000A102XF4\",\n" +
                        "    \"bid\": 120,\n" +
                        "    \"ask\": 130 \n" +
                        "}]\n")
                .characterEncoding("UTF-8"))
                .andExpect(status().isOk());

        mvc.perform(get("/RU000A102XF2"))
                .andExpect(status().isOk());

        mvc.perform(get("/RU000A102XF3"))
                .andExpect(status().isOk());

        mvc.perform(get("/RU000A102XF4"))
                .andExpect(status().isOk());
    }
}